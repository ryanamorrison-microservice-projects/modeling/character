// Character package defines a character interface.
package character

// Character interface definition.
type Character interface {
  New(name string, ownerID string, homeChronicleID string, isNPC bool) (characterID string)
  ID() (string)
  Owner() (string)
  HomeChronicle() (string)
  Name() (string)
  UpdateName(name string)
  Nature() (string)
  SetNature(nature string)
  Demeanor() (string)
  SetDemeanor(demeanor string)
  Status() (string)
  SetStatus(status string)
  IsNPC() (bool)
  XferOwnership(ownerID string)
  XferOwnershipToST(ownerID string)
  StructAsString(flattened string)
}
