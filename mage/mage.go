// Mage implements the interface from character.
package mage

import(
  "fmt"
  _ "gitlab.com/ryanamorrison-microservice-projects/character"
  "github.com/google/uuid"
)

// Mage implementing the Character interface
type Mage struct {
  characterID string
  ownerID string
  homeChronicleID string
  characterName string
  nature string
  demeanor string
  status string
  isNPC bool
  rulesVersion ?
}

// New function takes a characer name, owner as gamerID and the ChroniceID that the character 
// belongs to as strings and returns a characterID string (for logging or other functions)
func (m *Mage) New(name string, ownerID string, homeChronicleID string, isNPC bool) (characterID string) {
  newid := uuid.New()
  m.characterID = newid.String()
  m.characterName = name
  m.ownerID = ownerID
  m.homeChronicleID = homeChronicleID
  m.isNPC = isNPC
  return m.characterID
}

// Name function returns the character name as a string
func (m *Mage) Name() (string) {
  return m.characterName
}

// ID function returns the unique ID of the character as a string
// the character ID is static after it is set
func (m *Mage) ID() (string) {
  return m.characterID
}

// Owner function returns the owner's ID as a string
func (m *Mage) Owner() (string) {
  return m.ownerID
}

// HomeChronicle function returns the Chronicle the character 
// belongs to
func (m *Mage) HomeChronicle() (string) {
  return m.homeChronicleID
}

// UpdateName function allows for the character name to be
// updated after character creation
func (m *Mage) UpdateName(name string) {
  m.characterName = name
}

// Nature function returns the character's Nature
func (m *Mage) Nature() (string) {
  return m.nature
}

// SetNature function allows a character's Nature to be updated 
// (even though this ought to be rare event)
func (m *Mage) SetNature(nature string) {
  m.nature = nature
}
 
// Demeanor function returns the character's Demeanor
func (m *Mage) Demeanor() (string) {
  return m.demeanor
}

// SetDemeanor function allows a character's natural Demeanor to 
// be updated though this ought to happen infrequently
func (m *Mage) SetDemeanor(demeanor string) {
  m.demeanor = demeanor
}

// Status function returns the character's game status (e.g., Active)
func (m *Mage) Status() (string) {
  return m.status
}  

// SetStatus function changes the character's game status
// (e.g., Active, Harrowing, Torpor, Dead)
func (m *Mage) SetStatus(status string) {
  m.status = status
}

// IsNPC function returns whether the character is an NPC or not
// the default is false (they are a PC)
func (m *Mage) IsNPC() (bool) {
  return m.isNPC
}

// XferOwnership function allows a character to be tranferred from
// its current owner to another player and will set the isNPC field
// to false regardless of its current status
func (m *Mage) XferOwnership(ownerID string) {
  m.ownerID = ownerID
  m.isNPC = false
}

// XferOwnershipToST function allows a character to be transferred
// from its current owner to a Storyteller, thus making the character
// an NPC
func (m *Mage) XferOwnershipToST(ownerID string) {
  m.ownerID = ownerID
  m.isNPC = true
}

// StructAsString is a function to return a string representation 
// of the character's fields (e.g., for logging or serialization)
func (m *Mage) StructAsString() (flattened string) {
  return fmt.Sprintf("%#v", m)
}
